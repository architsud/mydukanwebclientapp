package com.mydukan.website.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mydukan.website.utilities.Constants;
import com.mydukan.website.utilities.Format;

@Controller
public class ClientController {

	/**
	 * @param model
	 * @return the index/home page for the MyDukan Front end Web Application
	 */
	@RequestMapping(value = { "/", "/home", "/index" })
	public String indexPage(Model model) {
		return "index";
	}

	/**
	 * @param model
	 * @return the login page for the MyDukan Front end Web Application
	 */
	@RequestMapping(value = { "/login" })
	public String loginPage(Model model) {
		return "login";
	}

	/**
	 * @param model
	 * @return the registration page for the MyDukan Front end Web Application
	 */
	@RequestMapping(value={"register/{userType}"}, method = RequestMethod.GET)
	public String registrationPage(Model model, @PathVariable("userType") String userType) {
		//userType can be "admin", �supplier�
		String userRole = null;
		if(Format.isStringEmptyOrNull(userType)) {
			if(!userType.equals(Constants.USER_TYPE_ADMIN) || userType.equals(Constants.USER_TYPE_SUPPLIER)) {
				userRole = userType;
			}else {
				userRole = Constants.USER_TYPE_SUPPLIER;
			}
		}
		model.addAttribute("userType", userRole);
		return "registration";
	}
	
	/**
	 * @param model
	 * @return the index/home page for the MyDukan Front end Web Application
	 */
	@RequestMapping(value = { "/test" })
	public String adminPage(Model model) {
		return "admin";
	}

}
