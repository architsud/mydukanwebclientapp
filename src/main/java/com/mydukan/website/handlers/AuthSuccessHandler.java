package com.mydukan.website.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component("authSuccessHandler")
public class AuthSuccessHandler implements AuthenticationSuccessHandler {

	private static final Logger logger = LoggerFactory.getLogger(AuthSuccessHandler.class);

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {
		
		System.out.println("Authenticaton is successful here.........");
		
		// try {
		//
		// // on successful tenant authentication, move to the correct page
		// // /admin will show the admin console of the tenant.
		// if (!(auth instanceof AnonymousAuthenticationToken)) {
		// UserInfo userInfo = (UserInfo)
		// SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		// if (userInfo != null) {
		// System.out.println(userInfo.getName());
		// redirectStrategy.sendRedirect(request, response, "/admin");
		// }
		// } else {
		// System.out.println("User is anonymous!");
		// }
		// } catch (Exception e) {
		// logger.error(e.getMessage(), e);
		// }

	}

}
