package com.mydukan.website.providers;

import java.util.ArrayList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.mydukan.website.validations.ValidatorImpl;

@Component
public class LoginAuthProvider implements AuthenticationProvider {

	private static final Logger logger = LoggerFactory.getLogger(LoginAuthProvider.class);

	@Autowired
	private ValidatorImpl validator;
	//
	// @Autowired
	// private UserClient userClient;

	public void setValidator(ValidatorImpl validator) {
		this.validator = validator;
	}

	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		System.out.println("In the authenticate function");

		String email = authentication.getName().trim();
		final String password = authentication.getCredentials().toString().trim();

		System.out.println("Username: " + email);
		System.out.println("Password: " + password);

		// try {
		// HttpServletRequest request = ((ServletRequestAttributes)
		// RequestContextHolder.currentRequestAttributes())
		// .getRequest();
		// String requestName = request.getServerName();
		//
		// if(requestName.split("\\.")[0].equals("admin")) { // handle the admin case
		//
		// } else if(requestName.split("\\.")[0].equals("mentor")) { // handle the
		// mentor case
		//
		// } else if (requestName.split("\\.")[0].equals("director")) { // handle the
		// director case
		//
		// } else if (requestName.split("\\.")[0] == null) { // handle the mentee case
		//
		// }
		//
		// String email = authentication.getName().trim();
		// final String password = authentication.getCredentials().toString().trim();
		//
		// if (!validator.validateEmail(email) || !validator.validatePassword(password))
		// {
		// System.out.println("Validation failed: " + email + " -> " + password);
		// return null;
		// }
		//
		// if (email.equals("admin@admin.com") && password.equals("Superm@n77_")) {
		// List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
		// grantedAuths.add(new SimpleGrantedAuthority(Constants.ADMIN_ACCESS));
		// return new UsernamePasswordAuthenticationToken(
		// new UserInfo("1", "Admin Name", email, "Admin contact", 'A'), password,
		// grantedAuths);
		// }
		// } catch (AuthenticationException e) {
		// logger.error(e.getMessage(), e);
		// } catch (Exception e) {
		// logger.error(e.getMessage(), e);
		// }
		return null;
	}

	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
