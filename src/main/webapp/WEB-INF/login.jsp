<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html ng-app="srApp">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>MyAppDukan - Login Interface</title>
<meta name="description" content="" />

<!-- Favicon -->
<link rel="shortcut icon"
	href="<c:out value="${pageContext.request.contextPath}/resources/images/favicon.png" />"
	type="image/x-icon" />
<link rel="icon"
	href="<c:out value="${pageContext.request.contextPath}/resources/images/favicon.png" />"
	type="image/x-icon" />

<!-- mobile settings -->
<meta name="viewport"
	content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<!-- WEB FONTS : use %7C instead of | (pipe) -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700"
	rel="stylesheet" type="text/css" />

<!-- FONT-AWESOME CSS -->
<link
	href="<c:out value="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.min.css" />"
	rel="stylesheet" type="text/css">

<!-- EXTERNAL ANGULAR CSS -->
<link rel="stylesheet"
	href="<c:out value="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.min.css" />" />
<link rel="stylesheet"
	href="<c:out value="${pageContext.request.contextPath}/libraries/ngToast/dist/ngToast.min.css" />" />
<link rel="stylesheet"
	href="<c:out value="${pageContext.request.contextPath}/libraries/ngToast/dist/ngToast-animations.min.css" />" />
<link rel="stylesheet"
	href="<c:out value="${pageContext.request.contextPath}/libraries/loadingbar/loading-bar.css" />" />

<!-- for the adminLTE theme -->
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
<link
	href="<c:out value="${pageContext.request.contextPath}/resources/adminLTE/css/AdminLTE.css" />"
	rel="stylesheet" type="text/css">
<link
	href="<c:out value="${pageContext.request.contextPath}/resources/adminLTE/css/skins/_all-skins.min.css" />"
	rel="stylesheet" type="text/css">

<!-- Ionicons from the AdminLTE -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

<!-- iCheck plugin from AdminLTE -->
<link
	href="<c:out value="${pageContext.request.contextPath}/resources/plugins/iCheck/square/blue.css" />"
	rel="stylesheet" type="text/css">

<!-- CUSTOM CSS -->
<link
	href="<c:out value="${pageContext.request.contextPath}/resources/adminLTE/css/custom.css" />"
	rel="stylesheet" type="text/css">

<!-- for the angular-bootstrap-toogle -->
<link
	href="<c:out value="${pageContext.request.contextPath}/libraries/angular-bootstrap-toggle/angular-bootstrap-toggle.min.css" />"
	rel="stylesheet">

</head>

<body class="hold-transition login-page" ng-controller="loginController">

	<div class="wrapper">

		<div ng-show="showDisabledScreen" class="load-overlay"></div>

		<toast></toast>

		<div class="login-box">
			<div class="login-logo">
				<a href="#">My<b>AppDukan</b></a>
			</div>
			<!-- /.login-logo -->
			<div class="login-box-body">
				<p class="login-box-msg">Sign in to start your session</p>

				<form action="login" method="post">
					<div class="form-group has-feedback">
						<input type="email" name="username" class="form-control"
							placeholder="Login Email"> <span
							class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" name="userpassword" class="form-control"
							placeholder="Login Password"> <span
							class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-8">
							<div class="checkbox icheck">
								<label> <input type="checkbox"> Remember Me
								</label>
							</div>
						</div>
						<!-- /.col -->
						<div class="col-xs-4">
							<sec:csrfInput />
							<button type="submit" class="btn btn-primary btn-block btn-flat">Sign
								In</button>
						</div>
						<!-- /.col -->
					</div>
				</form>

				<a href="#">I forgot my password</a><br> <a href="#"
					class="text-center">Register a new membership</a>

			</div>
			<!-- /.login-box-body -->
		</div>
		<!-- /.login-box -->


	</div>
	<!-- ./wrapper -->
</body>

<!-- Angular dependency from CDN -->
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
<!-- jQuery dependecy from CDN -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.5.8/angular-sanitize.min.js"></script>

<!-- Required for dataFactory.js con variable -->
<script type="text/javascript">
	var con = "${pageContext.request.contextPath}";

	jQuery(function() {
		jQuery('input').iCheck({
			checkboxClass : 'icheckbox_square-blue',
			radioClass : 'iradio_square-blue',
			increaseArea : '20%' // optional
		});
	});
</script>

<script
	src="<c:out value="${pageContext.request.contextPath}/app/appModule.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/app/appConfig.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/app/factories/dataFactory.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/app/factories/httpInterceptor.js" />"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

<!-- SlimScroll Plugin Dependency for the theme -->
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/slimScroll/jquery.slimscroll.min.js" />"></script>

<!-- Angular Dependencies -->
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/angular-ui-router/angular-ui-router.min.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/angular/angular-sanitize.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/ngToast/dist/ngToast.min.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/angular-validator/angular-validator.min.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/angular/angular-animate.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/loadingbar/loading-bar.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/angular-ui/ui-bootstrap-2.1.3.min.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/angular-ui/ui-bootstrap-tpls-2.1.3.min.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/angular-scroll/angular-scroll.min.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/angular-trix/angular-trix.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/angular-cookies/angular-cookies.min.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/angular-bootstrap-toggle/angular-bootstrap-toggle.min.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/libraries/dropdown-multiselect/angularjs-dropdown-multiselect.js" />"></script>

<!-- Bootstrap 3.3.7 -->
<script
	src="<c:out value="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap.min.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js" />"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}/resources/adminLTE/js/app.js" />"></script>

<!-- custom controllers for the components -->
<script
	src="<c:out value="${pageContext.request.contextPath}/app/pages/login/controller/loginController.js" />"></script>

<!-- custom services -->
<script
	src="<c:out value="${pageContext.request.contextPath}/app/services/preferenceService.js" />"></script>

<!-- For the directives -->
<script
	src="<c:out value="${pageContext.request.contextPath}/app/directives/compile.js" />"></script>

</html>



