<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html ng-app="srApp">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>MyAppDukan - The Web Interface</title>
<meta name="description" content="" />

<!-- Favicon -->
<link rel="shortcut icon" href="resources/images/favicon.png"
	type="image/x-icon" />
<link rel="icon" href="resources/images/favicon.png" type="image/x-icon" />

<!-- mobile settings -->
<meta name="viewport"
	content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<!-- WEB FONTS : use %7C instead of | (pipe) -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700"
	rel="stylesheet" type="text/css" />

<!-- CORE CSS FOR BOOTSTRAP -->
<link href="resources/bootstrap/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/bootstrap/css/bootstrap-theme.min.css"
	rel="stylesheet" type="text/css" />

<!-- FONT-AWESOME CSS -->
<link href="resources/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- EXTERNAL ANGULAR CSS -->
<link rel="stylesheet"
	href="resources/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="libraries/ngToast/dist/ngToast.min.css">
<link rel="stylesheet"
	href="libraries/ngToast/dist/ngToast-animations.min.css">
<link rel="stylesheet" href="libraries/loadingbar/loading-bar.css">

<!-- for the angular toggle button CSS -->
<link
	href="libraries/angular-bootstrap-toggle/angular-bootstrap-toggle.min.css"
	rel="stylesheet">

<!-- THEME CSS -->
<link href="resources/smarty/css/essentials.css" rel="stylesheet"
	type="text/css" />
<link href="resources/smarty/css/layout.css" rel="stylesheet"
	type="text/css" />

<!-- REVOLUTION SLIDER -->
<link
	href="resources/smarty/plugins/slider.revolution/css/extralayers.css"
	rel="stylesheet" type="text/css" />
<link href="resources/smarty/plugins/slider.revolution/css/settings.css"
	rel="stylesheet" type="text/css" />

<!-- PAGE LEVEL SCRIPTS -->
<link href="resources/smarty/css/header-1.css" rel="stylesheet"
	type="text/css" />
<link href="resources/smarty/css/color_scheme/blue.css" rel="stylesheet"
	type="text/css" id="color_scheme" />

</head>

<body id="home" class="smoothscroll" ng-controller="mainController">

	<div ng-show="showDisabledScreen" class="load-overlay"></div>

	<toast></toast>

	<!-- wrapper -->
	<div ng-cloak id="wrapper">

		<div ng-include="'app/components/FE/navHeader/navHeader.jsp'"></div>

		<div class="container">
			<h1 class="page-header">Index Page for the Test!!</h1>
		</div>

		<div ng-include="'app/components/FE/footer/footer.jsp'"></div>

	</div>
	<!-- /wrapper -->

	<!-- SCROLL TO TOP -->
	<a href="#" id="toTop"></a>

	<!-- Angular dependency from CDN -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
	<!-- jQuery dependecy from CDN -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.5.8/angular-sanitize.min.js"></script>
	<script src="app/appModule.js"></script>
	<script src="app/appConfig.js"></script>
	<script src="app/factories/dataFactory.js"></script>
	<script src="app/factories/httpInterceptor.js"></script>

	<!-- Angular Dependencies -->
	<script src="libraries/angular-ui-router/angular-ui-router.min.js"></script>
	<script src="libraries/angular/angular-sanitize.js"></script>
	<script src="libraries/ngToast/dist/ngToast.min.js"></script>
	<script src="libraries/angular-validator/angular-validator.min.js"></script>
	<script src="libraries/angular/angular-animate.js"></script>
	<script src="libraries/loadingbar/loading-bar.js"></script>
	<script src="libraries/angular-ui/ui-bootstrap-2.1.3.min.js"></script>
	<script src="libraries/angular-ui/ui-bootstrap-tpls-2.1.3.min.js"></script>
	<script src="libraries/angular-scroll/angular-scroll.min.js"></script>
	<script src="libraries/angular-trix/angular-trix.js"></script>
	<script src="libraries/angular-cookies/angular-cookies.min.js"></script>
	<script
		src="libraries/angular-bootstrap-toggle/angular-bootstrap-toggle.min.js"></script>
	<script
		src="libraries/dropdown-multiselect/angularjs-dropdown-multiselect.js"></script>

	<!-- Js Dependencies -->
	<script src="resources/bootstrap/js/bootstrap.min.js"></script>

	<!-- For all the custom controllers -->
	<script src="app/pages/index/controller/mainController.js"></script>
	<script src="app/components/FE/navHeader/navHeaderController.js"></script>
	<script src="app/components/FE/footer/footerController.js"></script>

	<!-- the modal dependencies -->
	<script src="app/modals/contentModal/contentModalController.js"></script>

	<script type="text/javascript">
		var con = '';
	</script>

	<!-- for the Smarty theme -->
	<script type="text/javascript">
		var plugin_path = 'resources/smarty/plugins/';
	</script>
	<script type="text/javascript"
		src="resources/smarty/plugins/jquery/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="resources/smarty/js/scripts.js"></script>

	<!-- REVOLUTION SLIDER -->
	<script type="text/javascript"
		src="resources/smarty/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript"
		src="resources/smarty/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>

</body>
</html>