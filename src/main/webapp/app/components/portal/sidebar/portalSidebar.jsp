<!-- Left side column. contains the logo and sidebar -->
<aside class="fixed main-sidebar"
	ng-controller="portalSidebarController">

	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar" ng-if="sidebar.enabled">

		<!-- Sidebar Menu -->
		<ul class="sidebar-menu" ng-repeat="menu in sidebar.menu.menuList" z>

			<!-- ng-href="{{menuItem.link}}" -->
			<li class="header" ng-bind-html="menu.header">{{menu.header}}</li>
			<li class="treeview" ng-repeat="menuItem in menu.menuItems"
				ng-if="menuItem.enabled"><a href
				ng-click="scroll(menuItem.scrollLink)"><i
					ng-class="menuItem.class"></i> <span ng-bind-html="menuItem.text">{{menuItem.text}}</span>
					<span class="pull-right-container"> <i
						class="fa fa-angle-right pull-right"></i>
				</span> </a> <!-- with the submenu link -->
				<ul class="treeview-menu"
					ng-repeat="subMenu in menuItem.subMenu.list"
					ng-if="menuItem.subMenu.enabled">
					<!-- ng-href="{{subMenu.link}}" -->
					<li><a ng-click="scroll(subMenu.scrollLink)"
						ng-bind-html="subMenu.text"><i ng-class="subMenu.class"></i>{{subMenu.text}}</a></li>
				</ul></li>

		</ul>
		<!-- /.sidebar-menu -->
	</section>
	<!-- /.sidebar -->
</aside>






