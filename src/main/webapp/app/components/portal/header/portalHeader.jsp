<!-- Main Header -->
<header class="main-header" ng-controller="portalHeaderController">

	<!-- Logo -->
	<a href class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>{{header.logo.condensedText}}</b></span> <!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>{{header.logo.boldText}}</b>{{header.logo.text}}</span>
	</a>

	<!-- Header Navbar -->
	<nav class="navbar navbar-static-top" role="navigation">

		<!-- Sidebar toggle button-->
		<a href class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a> 

		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">

				<!-- User Account Menu - in the end -->
				<li class="dropdown user user-menu">
					<!-- Menu Toggle Button --> <a href class="dropdown-toggle"
					data-toggle="dropdown"> <img ng-model="userIcon"
						ng-src="player/profile/image?imageId={{userIcon}}"
						class="user-image" alt="Premier-Recruits User Image"> <span
						class="hidden-xs" ng-bind-html="header.account.userName">{{header.account.userName}}</span>
				</a>

					<ul class="dropdown-menu">

						<li class="user-header"><img
							ng-src="player/profile/image?imageId={{userIcon}}"
							class="img-circle" alt="User Image">
							<p ng-bind-html="header.account.userName">
								{{header.account.userName}} <small
									ng-bind-html="header.account.text">{{header.account.text}}</small>
							</p></li>

						<!-- Menu Footer-->
						<li class="user-footer">
							<div ng-repeat="menu in header.account.subMenu.list"
								ng-class="menu.class" ng-if="header.account.subMenu.enabled">
								<a ng-href="{{menu.link}}" class="btn btn-default btn-flat"
									ng-bind-html="menu.text">{{menu.text}}</a>
							</div>

							<div class="pull-right">
								<a href ng-click="doLogout()" class="btn btn-default btn-flat">
									<form id="form-logout" name="form-logout" action="logout"
										method="POST" style="display: none;">
										<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" /> <input type="submit"
											value="Logout!" />
									</form> Logout
								</a>
							</div>
						</li>
					</ul>

				</li>
			</ul>
		</div>
	</nav>
</header>