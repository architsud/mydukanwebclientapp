angular.module('srApp').controller('portalHeaderController',
		portalHeaderController);

portalHeaderController.$inject = [ '$scope', '$rootScope', 'dataFactory',
		'ngToast', '$uibModal' ];

function portalHeaderController($scope, $rootScope, dataFactory, ngToast,
		$uibModal) {
	console.log("logging portalHeaderController");
	var vm = this;

	$scope.userIcon = "";

	vm.initAdminHeader = function initAdminHeader() {
		console.log("Logging portal Header");
		console.log($rootScope.header);
	};

	// // for logging out the admin user
	$scope.doLogout = function doLogout() {
		document.getElementById('form-logout').submit();
	};

	// update the userIcon in the header
	$scope.showUserIcon = function showUserIcon(image) {
		$scope.userIcon = image;
	};

	// the $on function for updating the locations from any other controller
	$rootScope.$on("updateUserIcon", function(event, data) {
		$scope.showUserIcon(data.image);
	});

	vm.initAdminHeader();

}