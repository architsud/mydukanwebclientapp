angular.module('srApp').controller('bladeController', bladeController);

bladeController.$inject = [ '$scope', '$rootScope', 'dataFactory', 'ngToast',
		'$location', 'remoteService' ];

function bladeController($scope, $rootScope, dataFactory, ngToast, $location,
		remoteService) {
	var vm = this;

	$rootScope.headerAlert = {};

	// for loading the static content from the json files
	vm.initAdminBlade = function initAdminBlade() {
		console.log("In the bladeController");
		console.log($rootScope.sidebar);

		// this is for scrolling of the fixed sidebar
		jQuery.AdminLTE.layout.activate();
		jQuery.AdminLTE.layout.fix();
		jQuery.AdminLTE.layout.fixSidebar();

		vm.headerAlertContent = dataFactory
				.getPageContents("pages/common/content/alertContent")
				.then(
						function(response) {
							$rootScope.headerAlert = response.data.portal;

							// get all the queryStrings and show the required
							// messages
							var status = $location.search().status;
							if (status == '1') {
								if ($location.search().profile == '1') {
									$rootScope.headerAlert.success
											.push({
												"enabled" : true,
												"class" : 'alert-success',
												"enableCloseButton" : true,
												"content" : 'Profile Information Updated Successfully. Updated Information will be available after logging in again'
											});
								}
							} else if (status == '0') {
								if ($location.search().profile = '0'
										&& $location.search().img == '0') {
									$rootScope.headerAlert.warning
											.push({
												"enabled" : true,
												"class" : 'alert-warning',
												"enableCloseButton" : true,
												"content" : 'Only .png, .jpg and .jpeg formats are supported. Please upload the profile picture again'
											});
								} else if ($location.search().profile = '0'
										&& $location.search().input == '0') {
									$rootScope.headerAlert.warning
											.push({
												"enabled" : true,
												"class" : 'alert-warning',
												"enableCloseButton" : true,
												"content" : 'Seems like your input was not Legit. Please try again with the correct input'
											});
								} else if ($location.search().profile = '0') {
									$rootScope.headerAlert.danger
											.push({
												"enabled" : true,
												"class" : 'alert-danger',
												"enableCloseButton" : true,
												"content" : 'Hit an Error while serving. Please try again'
											});
								}
							}

						},
						function(response) {
							ngToast
									.create({
										className : 'danger',
										content : 'Content could not be loaded. Please try again'
									});
						});

	};

	vm.initAdminBlade();

};