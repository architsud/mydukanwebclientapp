<!-- Content Wrapper. Contains page content -->

<!-- blade-container-full -->

<div class="content-wrapper" ng-controller="bladeController">

	<!-- for the alert thing for all the pages -->
	<div class="alert alert-success alert-dismissible alert-portal"
		ng-repeat="alert in headerAlert.success" ng-if="alert.enabled">
		<button ng-if="alert.enableCloseButton" type="button" class="close"
			data-dismiss="alert" aria-hidden="true">&times;</button>
		<span ng-bind-html="alert.content">{{alert.content}}</span>
	</div>

	<div class="alert alert-info alert-dismissible alert-portal"
		ng-repeat="alert in headerAlert.info" ng-if="alert.enabled">
		<button ng-if="alert.enableCloseButton" type="button" class="close"
			data-dismiss="alert" aria-hidden="true">&times;</button>
		<span ng-bind-html="alert.content">{{alert.content}}</span>
	</div>

	<div class="alert alert-warning alert-dismissible alert-portal"
		ng-repeat="alert in headerAlert.warning" ng-if="alert.enabled">
		<button ng-if="alert.enableCloseButton" type="button" class="close"
			data-dismiss="alert" aria-hidden="true">&times;</button>
		<span ng-bind-html="alert.content">{{alert.content}}</span>
	</div>

	<div class="alert alert-danger alert-dismissible alert-portal"
		ng-repeat="alert in headerAlert.danger" ng-if="alert.enabled">
		<button ng-if="alert.enableCloseButton" type="button" class="close"
			data-dismiss="alert" aria-hidden="true">&times;</button>
		<span ng-bind-html="alert.content">{{alert.content}}</span>
	</div>

	<!-- for the menu and non-menu UIs visible as modules -->
	<div ui-view="blade_menu_ui"></div>
	<div ui-view="blade_ui"></div>

</div>
<!-- /.content-wrapper -->

