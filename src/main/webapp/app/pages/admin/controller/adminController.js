angular.module('srApp').controller('adminController', adminController);

adminController.$inject = [ '$scope', '$rootScope', 'dataFactory', 'ngToast',
		'$location', 'remoteService', '$uibModal', '$state' ];

function adminController($scope, $rootScope, dataFactory, ngToast, $location,
		remoteService, $uibModal, $state) {
	var vm = this;

	// this is necessary for the API calls to work
	$rootScope.authToken = "";

	$rootScope.showDisabledScreen = true;
	// On starting of the loading bar during any AJAX request
	$rootScope.$on('cfpLoadingBar:started', function(event, data) {
		$rootScope.showDisabledScreen = true;
	});
	// on ending of the loading bar during any AJAX request
	$rootScope.$on('cfpLoadingBar:completed', function(event, data) {
		$rootScope.showDisabledScreen = false;
	});

	// global functions for common functionality in all the components
	$rootScope.showBoxImage = function showBoxImage(imageName) {
		if (imageName == '' || imageName == undefined || imageName == null
				|| imageName == 'null') {
			return false;
		}
		return true;
	}

	// for opening the modal with the correct address.
	$scope.showModal = function showModal(context) {
		// to call the open Modal function to open the given model if required
	};

	// common function to open the modal with the given parameters
	$rootScope.openModal = function openModal(modalLocation, modalCtrl, size,
			data) {
		var modalInstance = $uibModal.open({
			animation : true,
			templateUrl : modalLocation,
			controller : modalCtrl,
			size : size,
			resolve : {
				data : function() {
					return data;
				}
			}
		});
	};

	// for loading the static content from the json files
	vm.initPlayer = function initPlayer() {

		console.log("Logging player Controller");

		// get the static contents
		vm.playerStaticContent = dataFactory.getPageContents(
				"pages/admin/content/adminContent").then(function(response) { // success
			// case
			$rootScope.header = response.data.header;
			$rootScope.sidebar = response.data.sidebar;
			$rootScope.footer = response.data.footer;

			// // getting the data from the UserCheckin Remote API
			// // Call
			// remoteService
			// .getUserCheckinDetails('player')
			// .getUserData()
			// .then(
			// function(response) {
			// $rootScope.header.account.userName = "";
			// if (response.data.userInfo.userName != null
			// && response.data.userInfo.userName != ''
			// && response.data.userInfo.userName != 'null'
			// && response.data.userInfo.userName != undefined)
			// $rootScope.header.account.userName =
			// response.data.userInfo.userName;
			//
			// $rootScope.header.account.userImg = "";
			// if (response.data.userInfo.userImg != null
			// && response.data.userInfo.userImg != ''
			// && response.data.userInfo.userImg != 'null'
			// && response.data.userInfo.userImg != undefined)
			// $rootScope.header.account.userImg =
			// response.data.userInfo.userImg;
			//
			// $rootScope.header.account.userDesc = "";
			// if (response.data.userInfo.userDesc != undefined
			// && response.data.userInfo.userDesc != null
			// && response.data.userInfo.userDesc != 'null'
			// && response.data.userInfo.userDesc != '') {
			// $rootScope.header.account.userDesc =
			// response.data.userInfo.userDesc;
			// }
			//
			// $rootScope.header.account.userContact = "";
			// if (response.data.userInfo.userContact != undefined
			// && response.data.userInfo.userContact != null
			// && response.data.userInfo.userContact != 'null'
			// && response.data.userInfo.userContact != '') {
			// $rootScope.header.account.userContact =
			// response.data.userInfo.userContact;
			// }
			//
			// $rootScope.authToken = response.data.authToken;
			// $rootScope.visibleToAll = response.data.userInfo.visibleToAll;
			//
			// // to update the user icon in
			// // the header
			// $rootScope
			// .$emit(
			// "updateUserIcon",
			// {
			// 'image' : $rootScope.header.account.userImg
			// });
			//
			// },
			// function(response) {
			// if (response.status == 400) {
			// ngToast
			// .create({
			// className : 'warning',
			// content : 'UnAuthorized Access. Plase <a href="login">Login
			// Again</a>'
			// });
			// } else {
			// ngToast
			// .create({
			// className : 'danger',
			// content : response.data.message
			// });
			// }
			// });

		}, function(response) { // fail case
			ngToast.create({
				className : 'danger',
				content : 'Content could not be loaded. Please try again'
			});
		});

		// to redirect to the player dashboard page
		// $location.path('/player/profile');

	};

	// for the include content loaded event
	$rootScope.$on('$includeContentLoaded', function(evt, templateName) {
		console.log("Loaded!" + templateName);

	});

	// for the default view in case of the internal menu option
	$rootScope.showDefaultView = function showDefaultView(url) {
		if (url == $location.url())
			return true;
		return false;
	};

	vm.initPlayer();

};
