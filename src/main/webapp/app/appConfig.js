/**
 * Routing for the complete web app
 */
angular
		.module('srApp')
		.config(
				[
						'$stateProvider',
						'$urlRouterProvider',
						'$httpProvider',
						'cfpLoadingBarProvider',
						'ngToastProvider',
						function($stateProvider, $urlRouterProvider,
								$httpProvider, cfpLoadingBarProvider,
								ngToastProvider) {

							// for registering the http interceptor
							$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
							$httpProvider.interceptors.push('httpInterceptor');

							// for the loading spinner
							cfpLoadingBarProvider.includeSpinner = true;

							// for the ngToast configuration
							ngToastProvider.configure({
								animation : 'slide'
							});

							// application routes for all the modules and login
							// requiring pages
							$stateProvider
									.state(
											'adminDashboard',
											{
												url : '/admin/dashboard',
												views : {
													'blade_ui' : {
														templateUrl : 'app/components/admin/dashboard/adminDashboard.jsp',
														controller : 'adminDashboardController'
													}
												}
											});

						} ]);

// unused states, might be used in future
// .state(
// 'playerDashboard',
// {
// url : '/player/dashboard',
// views : {
// 'blade_ui' : {
// templateUrl : 'app/components/player/dashboard/playerDashboard.jsp',
// controller : 'playerDashboardController'
// }
// },
// params : {
// reload : true
// }
// })
// .state(
// 'playerClubListing',
// {
// url : '/player/listing/club',
// views : {
// 'blade_ui' : {
// templateUrl : 'app/components/player/clubListing/clubListing.jsp',
// controller : 'clubListingController'
// }
// },
// params : {
// reload : true
// }
// })
// .state(
// 'playerPreference',
// {
// url : '/player/preference',
// views : {
// 'blade_ui' : {
// templateUrl : 'app/components/player/preference/preference.jsp',
// controller : 'preferenceController'
// }
// }
// })
// .state(
// 'playerPlayingPreference',
// {
// url : '/player/settings',
// views : {
// 'blade_ui' : {
// templateUrl :
// 'app/components/player/playingPreference/playingPreference.jsp',
// controller : 'playingPreferenceController'
// }
// }
// })
// .state(
// 'playerSocial',
// {
// url : '/player/social',
// views : {
// 'blade_ui' : {
// templateUrl : 'app/components/common/social/social.jsp',
// controller : 'socialController'
// }
// }
// })
// .state(
// 'playerCareer',
// {
// url : '/player/career',
// views : {
// 'blade_ui' : {
// templateUrl : 'app/components/player/career/playerCareer.jsp',
// controller : 'playerCareerController'
// }
// }
// })
